/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef MCAL_DRIVERS_I2C_DRIVER_I2C_FUNCTIONS_H_
#define MCAL_DRIVERS_I2C_DRIVER_I2C_FUNCTIONS_H_


#include "../../basic_includes/custom_types.h"

// the address 0000 000 is reserved for a general call

// Master & Slave Status/Action codes
typedef enum
{
    // master
    I2C_master_action_Start                     = 0x08,
    I2C_master_action_Repeated_Start            = 0x10,

    I2C_master_action_SLA_W_transmitted_ACK     = 0x18,
    I2C_master_action_SLA_W_transmitted_NACK    = 0x20,

    I2C_master_action_SLA_R_transmitted_ACK     = 0x40,
    I2C_master_action_SLA_R_transmitted_NACK    = 0x48,

    I2C_master_action_Data_transmitted_ACK      = 0x28,
    I2C_master_action_Data_transmitted_NACK     = 0x30,

    I2C_master_action_Data_received_ACK         = 0x50,
    I2C_master_action_Data_received_NACK        = 0x58,

    I2C_master_action_Arbitration_lost          = 0x38,


    // slave
    I2C_slave_action_Stop_or_ReStart_received                = 0xA0,

    I2C_slave_action_SLA_W_received_ACK                      = 0x60,

    I2C_slave_action_SLA_R_received_ACK                      = 0xA8,

    I2C_slave_action_General_call_received_ACK               = 0x70,

    I2C_slave_action_data_received_after_SLA_W_ACK           = 0x80,
    I2C_slave_action_data_received_after_SLA_W_NACK          = 0x88,

    I2C_slave_action_data_received_after_General_call_ACK    = 0x90,
    I2C_slave_action_data_received_after_General_call_NACK   = 0x98,

    I2C_slave_action_Data_transmitted_ACK                    = 0xB8,
    I2C_slave_action_Data_transmitted_NACK                   = 0xC0,
    I2C_slave_action_Data_transmitted_NO_TWEA_ACK            = 0xC8,

    I2C_slave_action_Arbitration_lost_own_SLA_W_received_ACK = 0x68,
    I2C_slave_action_Arbitration_lost_own_SLA_R_received_ACK = 0xB0,
    I2C_slave_action_Arbitration_lost_General_call_ACK       = 0x78,


	// error
	I2C_action_No_relevant_state_info 		   = 0xF8,
	I2C_action_Bus_error_illegal_Start_or_Stop = 0
} I2C_action_t;

// used by callbacks (master or slave) to determine
// what happens in next action
typedef enum
{
    I2C_next_action_ACK,
    I2C_next_action_NACK,
    I2C_next_action_Stop_condition,
    I2C_next_action_Start_condition,
    I2C_next_action_Stop_then_Start_conditions,

    I2C_next_action_NONE
} I2C_next_action_t;

typedef I2C_next_action_t (*I2C_CB_t)(void); // Master/Slave callback

// --- Master and Slave --- //
void I2C_vidEnable(void);
void I2C_vidDisable(void);

void I2C_vidEnableINT(void);
void I2C_vidDisableINT(void);
u8 I2C_u8isINTenabled(void);

void I2C_vidSetDeviceAddr(const u8 u8Addr, const u8 u8isRespondGenCall);
u8 I2C_u8GetDeviceAddr(void);

void I2C_vidEnableAck(void);
void I2C_vidDisableAck(void);
u8 I2C_u8isAckEnabled(void);

void I2C_vidRegisterCB(const I2C_CB_t CBfuncCpy, I2C_action_t enumActionCpy);
void I2C_vidDeregisterCB(I2C_action_t enumActionCpy);

void I2C_vidSetValue(const u8 u8DataCpy);
u8 I2C_u8GetValue(void);

I2C_action_t I2C_enumMasterGetLastAction(void);
// ------------------------ //


// --- Master --- //
void I2C_vidMasterInit(u32 u32ClockCpy); // step 0)

u8 I2C_u8MasterStartCond(void); // step 1)

// *** Master Tx *** //
u8 I2C_u8MasterSetSlaveAddrW(const u8 u8AddrCpy); // step 2)
u8 I2C_u8MasterSendData(const u8 u8DataCpy); // step 3)

// ***************** //

// *** Master Rx *** //
u8 I2C_u8MasterSetSlaveAddrR(const u8 u8AddrCpy); // step 2)
u8 I2C_u8MasterGetDataByTrigger(const u8 u8isAckCpy); // step 3)

// ***************** //

void I2C_vidMasterStopCond(const u8 u8isAckCpy); // step 4)
// -------------- //


// --- Slave --- //
void I2C_vidSlaveInit(const u8 u8Addr, const u8 u8isRespondGenCall); // step 0)

u8 I2C_u8SlaveSendDataByTrigger(const u8 u8DataCpy);

void I2C_vidSlaveRecover(void);
// ------------- //


#endif /* MCAL_DRIVERS_I2C_DRIVER_I2C_FUNCTIONS_H_ */

