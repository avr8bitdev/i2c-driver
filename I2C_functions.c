/*
  MIT License

  Copyright (c) 2018 Mina Helmi

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be
  included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "I2C_functions.h"
#include "../Peripherals_addresses.h"
#include "../DIO_driver/DIO_functions.h"
#include "../../basic_includes/bit_manip.h"
#include <avr/interrupt.h>


// --- internal --- //
static u8 I2C_isSlave = 0; // used by ISR to do a bus "handshake" when something is received

// master callbacks
static I2C_CB_t I2C_CB_master_Start = 0;
static I2C_CB_t I2C_CB_master_ReStart = 0;

static I2C_CB_t I2C_CB_master_SLAW_transmitted_ACK = 0;
static I2C_CB_t I2C_CB_master_SLAW_transmitted_NACK = 0;

static I2C_CB_t I2C_CB_master_SLAR_transmitted_ACK = 0;
static I2C_CB_t I2C_CB_master_SLAR_transmitted_NACK = 0;

static I2C_CB_t I2C_CB_master_Data_transmitted_ACK = 0;
static I2C_CB_t I2C_CB_master_Data_transmitted_NACK = 0;

static I2C_CB_t I2C_CB_master_Data_received_ACK = 0;
static I2C_CB_t I2C_CB_master_Data_received_NACK = 0;

static I2C_CB_t I2C_CB_master_Arbitration_lost = 0;

// slave callbacks
static I2C_CB_t I2C_CB_slave_Stop_or_ReStart_received = 0;

static I2C_CB_t I2C_CB_slave_SLA_W_received_ACK = 0;

static I2C_CB_t I2C_CB_slave_SLA_R_received_ACK = 0;

static I2C_CB_t I2C_CB_slave_General_call_received_ACK = 0;

static I2C_CB_t I2C_CB_slave_data_received_after_SLA_W_ACK = 0;
static I2C_CB_t I2C_CB_slave_data_received_after_SLA_W_NACK = 0;

static I2C_CB_t I2C_CB_slave_data_received_after_General_call_ACK = 0;
static I2C_CB_t I2C_CB_slave_data_received_after_General_call_NACK = 0;

static I2C_CB_t I2C_CB_slave_Data_transmitted_ACK = 0;
static I2C_CB_t I2C_CB_slave_Data_transmitted_NACK = 0;
static I2C_CB_t I2C_CB_slave_Data_transmitted_NO_TWEA_ACK = 0;

static I2C_CB_t I2C_CB_slave_Arbitration_lost_own_SLA_W_received_ACK = 0;
static I2C_CB_t I2C_CB_slave_Arbitration_lost_own_SLA_R_received_ACK = 0;
static I2C_CB_t I2C_CB_slave_Arbitration_lost_General_call_ACK = 0;

// error callbacks
static I2C_CB_t I2C_CB_No_relevant_state_info = 0;

static inline void I2C_vidActionInit(void)
{
    BIT_SET(I2C_CTRL, 7);
}

// TODO: this hangs when bus isn't released (slave didn't trigger)
//       make it counter based, and boolean of what happened
static inline void I2C_vidWaitWhileBusy(void)
{
    while (!BIT_GET(I2C_CTRL, 7)) // while action is not finished
    {}
}
// ---------------- //

// --- Master and Slave --- //
inline void I2C_vidEnable(void)
{
    BIT_SET(I2C_CTRL, 2); // activate I2C
}

inline void I2C_vidDisable(void)
{
    BIT_CLEAR(I2C_CTRL, 2); // deactivate I2C
}

inline void I2C_vidEnableINT(void)
{
    BIT_SET(I2C_CTRL, 0);
}

inline void I2C_vidDisableINT(void)
{
    BIT_CLEAR(I2C_CTRL, 0);
}

inline u8 I2C_u8isINTenabled(void)
{
    return BIT_GET(I2C_CTRL, 0);
}

void I2C_vidSetDeviceAddr(const u8 u8Addr, const u8 u8isRespondGenCall)
{
    I2C_ADDRESS = (u8Addr << 1);
    BIT_ASSIGN(I2C_ADDRESS, 0, u8isRespondGenCall);
}

inline u8 I2C_u8GetDeviceAddr(void)
{
    return (I2C_ADDRESS >> 1); // bits7-1 are device addr, bit0 is general call
}

inline void I2C_vidEnableAck(void)
{
    BIT_SET(I2C_CTRL, 6); // enable ACK bit
}

inline void I2C_vidDisableAck(void)
{
    // this also disconnects you from bus
    BIT_CLEAR(I2C_CTRL, 6); // disable ACK bit
}

inline u8 I2C_u8isAckEnabled(void)
{
    return BIT_GET(I2C_CTRL, 6);
}

void I2C_vidRegisterCB(const I2C_CB_t CBfuncCpy, I2C_action_t enumActionCpy)
{
    switch (enumActionCpy)
    {
        // master
        case I2C_master_action_Start:
            I2C_CB_master_Start = CBfuncCpy;
        break;

        case I2C_master_action_Repeated_Start:
            I2C_CB_master_ReStart = CBfuncCpy;
        break;

        case I2C_master_action_SLA_W_transmitted_ACK:
            I2C_CB_master_SLAW_transmitted_ACK = CBfuncCpy;
        break;

        case I2C_master_action_SLA_W_transmitted_NACK:
            I2C_CB_master_SLAW_transmitted_NACK = CBfuncCpy;
        break;

        case I2C_master_action_SLA_R_transmitted_ACK:
            I2C_CB_master_SLAR_transmitted_ACK = CBfuncCpy;
        break;

        case I2C_master_action_SLA_R_transmitted_NACK:
            I2C_CB_master_SLAR_transmitted_NACK = CBfuncCpy;
        break;

        case I2C_master_action_Data_transmitted_ACK:
            I2C_CB_master_Data_transmitted_ACK = CBfuncCpy;
        break;

        case I2C_master_action_Data_transmitted_NACK:
            I2C_CB_master_Data_transmitted_NACK = CBfuncCpy;
        break;

        case I2C_master_action_Data_received_ACK:
            I2C_CB_master_Data_received_ACK = CBfuncCpy;
        break;

        case I2C_master_action_Data_received_NACK:
            I2C_CB_master_Data_received_NACK = CBfuncCpy;
        break;

        case I2C_master_action_Arbitration_lost:
            I2C_CB_master_Arbitration_lost = CBfuncCpy;
        break;


        // slave
        case I2C_slave_action_Stop_or_ReStart_received:
            I2C_CB_slave_Stop_or_ReStart_received = CBfuncCpy;
        break;

        case I2C_slave_action_SLA_W_received_ACK:
            I2C_CB_slave_SLA_W_received_ACK = CBfuncCpy;
        break;

        case I2C_slave_action_SLA_R_received_ACK:
            I2C_CB_slave_SLA_R_received_ACK = CBfuncCpy;
        break;

        case I2C_slave_action_General_call_received_ACK:
            I2C_CB_slave_General_call_received_ACK = CBfuncCpy;
        break;

        case I2C_slave_action_data_received_after_SLA_W_ACK:
            I2C_CB_slave_data_received_after_SLA_W_ACK = CBfuncCpy;
        break;

        case I2C_slave_action_data_received_after_SLA_W_NACK:
            I2C_CB_slave_data_received_after_SLA_W_NACK = CBfuncCpy;
        break;

        case I2C_slave_action_data_received_after_General_call_ACK:
            I2C_CB_slave_data_received_after_General_call_ACK = CBfuncCpy;
        break;

        case I2C_slave_action_data_received_after_General_call_NACK:
            I2C_CB_slave_data_received_after_General_call_NACK = CBfuncCpy;
        break;

        case I2C_slave_action_Data_transmitted_ACK:
            I2C_CB_slave_Data_transmitted_ACK = CBfuncCpy;
        break;

        case I2C_slave_action_Data_transmitted_NACK:
            I2C_CB_slave_Data_transmitted_NACK = CBfuncCpy;
        break;

        case I2C_slave_action_Data_transmitted_NO_TWEA_ACK:
            I2C_CB_slave_Data_transmitted_NO_TWEA_ACK = CBfuncCpy;
        break;

        case I2C_slave_action_Arbitration_lost_own_SLA_W_received_ACK:
            I2C_CB_slave_Arbitration_lost_own_SLA_W_received_ACK = CBfuncCpy;
        break;

        case I2C_slave_action_Arbitration_lost_own_SLA_R_received_ACK:
            I2C_CB_slave_Arbitration_lost_own_SLA_R_received_ACK = CBfuncCpy;
        break;

        case I2C_slave_action_Arbitration_lost_General_call_ACK:
            I2C_CB_slave_Arbitration_lost_General_call_ACK = CBfuncCpy;
        break;


        // error
        case I2C_action_No_relevant_state_info:
            I2C_CB_No_relevant_state_info = CBfuncCpy;
        break;

        case I2C_action_Bus_error_illegal_Start_or_Stop:
        break;
    }

}

void I2C_vidDeregisterCB(I2C_action_t enumActionCpy)
{
    switch (enumActionCpy)
    {
        // master
        case I2C_master_action_Start:
            I2C_CB_master_Start = 0;
        break;

        case I2C_master_action_Repeated_Start:
            I2C_CB_master_ReStart = 0;
        break;

        case I2C_master_action_SLA_W_transmitted_ACK:
            I2C_CB_master_SLAW_transmitted_ACK = 0;
        break;

        case I2C_master_action_SLA_W_transmitted_NACK:
            I2C_CB_master_SLAW_transmitted_NACK = 0;
        break;

        case I2C_master_action_SLA_R_transmitted_ACK:
            I2C_CB_master_SLAR_transmitted_ACK = 0;
        break;

        case I2C_master_action_SLA_R_transmitted_NACK:
            I2C_CB_master_SLAR_transmitted_NACK = 0;
        break;

        case I2C_master_action_Data_transmitted_ACK:
            I2C_CB_master_Data_transmitted_ACK = 0;
        break;

        case I2C_master_action_Data_transmitted_NACK:
            I2C_CB_master_Data_transmitted_NACK = 0;
        break;

        case I2C_master_action_Data_received_ACK:
            I2C_CB_master_Data_received_ACK = 0;
        break;

        case I2C_master_action_Data_received_NACK:
            I2C_CB_master_Data_received_NACK = 0;
        break;

        case I2C_master_action_Arbitration_lost:
            I2C_CB_master_Arbitration_lost = 0;
        break;


        // slave
        case I2C_slave_action_Stop_or_ReStart_received:
            I2C_CB_slave_Stop_or_ReStart_received = 0;
        break;

        case I2C_slave_action_SLA_W_received_ACK:
            I2C_CB_slave_SLA_W_received_ACK = 0;
        break;

        case I2C_slave_action_SLA_R_received_ACK:
            I2C_CB_slave_SLA_R_received_ACK = 0;
        break;

        case I2C_slave_action_General_call_received_ACK:
            I2C_CB_slave_General_call_received_ACK = 0;
        break;

        case I2C_slave_action_data_received_after_SLA_W_ACK:
            I2C_CB_slave_data_received_after_SLA_W_ACK = 0;
        break;

        case I2C_slave_action_data_received_after_SLA_W_NACK:
            I2C_CB_slave_data_received_after_SLA_W_NACK = 0;
        break;

        case I2C_slave_action_data_received_after_General_call_ACK:
            I2C_CB_slave_data_received_after_General_call_ACK = 0;
        break;

        case I2C_slave_action_data_received_after_General_call_NACK:
            I2C_CB_slave_data_received_after_General_call_NACK = 0;
        break;

        case I2C_slave_action_Data_transmitted_ACK:
            I2C_CB_slave_Data_transmitted_ACK = 0;
        break;

        case I2C_slave_action_Data_transmitted_NACK:
            I2C_CB_slave_Data_transmitted_NACK = 0;
        break;

        case I2C_slave_action_Data_transmitted_NO_TWEA_ACK:
            I2C_CB_slave_Data_transmitted_NO_TWEA_ACK = 0;
        break;

        case I2C_slave_action_Arbitration_lost_own_SLA_W_received_ACK:
            I2C_CB_slave_Arbitration_lost_own_SLA_W_received_ACK = 0;
        break;

        case I2C_slave_action_Arbitration_lost_own_SLA_R_received_ACK:
            I2C_CB_slave_Arbitration_lost_own_SLA_R_received_ACK = 0;
        break;

        case I2C_slave_action_Arbitration_lost_General_call_ACK:
            I2C_CB_slave_Arbitration_lost_General_call_ACK = 0;
        break;


        // error
        case I2C_action_No_relevant_state_info:
            I2C_CB_No_relevant_state_info = 0;
        break;

        case I2C_action_Bus_error_illegal_Start_or_Stop:
        break;
    }

}

inline void I2C_vidSetValue(const u8 u8DataCpy)
{
    I2C_DATA = u8DataCpy;
}

inline u8 I2C_u8GetValue(void)
{
    return I2C_DATA;
}

inline I2C_action_t I2C_enumMasterGetLastAction(void)
{
    return (I2C_STATUS & 0b11111000); // prescaler and reserved bits are masked
}
// ------------------------ //


// --- Master --- //
void I2C_vidMasterInit(u32 u32ClockCpy) // step 0)
{
    // max clock = 400kHz (sanity check)
    if (u32ClockCpy > 400000UL)
        return;

    I2C_vidDisable();

    // prescaler bits (div factor = 1)
    BIT_ASSIGN(I2C_STATUS, 0, 0);
    BIT_ASSIGN(I2C_STATUS, 1, 0);

    // bit rate setting
    // ((F_CPU / F_I2C) - 16) / (2 * prescaler_div_factor)
    I2C_BIT_RATE = ( ((u32)F_CPU / u32ClockCpy) / 2 ) - 8;

    // enable ACK bit
    I2C_vidEnableAck();

    // reset callback functions
    // master callbacks
    I2C_CB_master_Start = 0;
    I2C_CB_master_ReStart = 0;
    I2C_CB_master_SLAW_transmitted_ACK = 0;
    I2C_CB_master_SLAW_transmitted_NACK = 0;
    I2C_CB_master_SLAR_transmitted_ACK = 0;
    I2C_CB_master_SLAR_transmitted_NACK = 0;
    I2C_CB_master_Data_transmitted_ACK = 0;
    I2C_CB_master_Data_transmitted_NACK = 0;
    I2C_CB_master_Data_received_ACK = 0;
    I2C_CB_master_Data_received_NACK = 0;
    I2C_CB_master_Arbitration_lost = 0;
    
    // error callbacks
    I2C_CB_No_relevant_state_info = 0;

    // deactivate internal pull-ups
    DIO_vidSet_pinDirection(port_C, 0, INPUT); // SCL
    DIO_vidDeactivate_pinPullUp(port_C, 0);
    DIO_vidSet_pinDirection(port_C, 1, INPUT); // SDA
    DIO_vidDeactivate_pinPullUp(port_C, 1);

    I2C_vidEnable();

    I2C_isSlave = 0;
}

u8 I2C_u8MasterStartCond(void) // step 1)
{
    BIT_SET(I2C_CTRL, 5); // set Start flag

    I2C_vidActionInit(); // reset action flag to initiate new one
    I2C_vidWaitWhileBusy(); // wait while busy

    register I2C_action_t last_action = I2C_enumMasterGetLastAction();

    // Very important note: after clearing this flag,
    // status register gives value = 0xF8, which in datasheet means:
    // "No relevant state information available; TWINT = 0"
    // and also raises the Collision flag, eventually screwing up the whole I2C,
    // hence, this flag MUST be cleared by software, ONLY AFTER writing data
    // or addr to I2C_DATA, AND THEN you must do the usual:
    // reset action flag to initiate new one then wait while busy

    // DANGEROUS: NEVER UNCOMMENT THIS LINE.
    //BIT_CLEAR(I2C_CTRL, 5); // clear Start flag

    return (    (last_action == I2C_master_action_Start)
             || (last_action == I2C_master_action_Repeated_Start) );
}

// *** Master Tx *** //
u8 I2C_u8MasterSetSlaveAddrW(const u8 u8AddrCpy) // step 2)
{// the address 0000 000 is reserved for a general call
    if (u8AddrCpy > 127) // valid addresses are 7-bits only!
        return 0;

    I2C_vidSetValue( (u8AddrCpy << 1) & 0b11111110 ); // load address in data register (write mode)

    BIT_CLEAR(I2C_CTRL, 5); // clear Start flag

    I2C_vidActionInit(); // reset action flag to initiate new one
    I2C_vidWaitWhileBusy(); // wait while busy

    return (I2C_enumMasterGetLastAction() == I2C_master_action_SLA_W_transmitted_ACK);
}

u8 I2C_u8MasterSendData(const u8 u8DataCpy) // step 3)
{
    I2C_vidSetValue(u8DataCpy); // load data in data register

    BIT_CLEAR(I2C_CTRL, 5); // clear Start flag

    I2C_vidActionInit(); // reset action flag to initiate new one
    I2C_vidWaitWhileBusy(); // wait while busy

    return (I2C_enumMasterGetLastAction() == I2C_master_action_Data_transmitted_ACK);
}
// ***************** //

// *** Master Rx *** //
u8 I2C_u8MasterSetSlaveAddrR(const u8 u8AddrCpy) // step 2)
{// the address 0000 000 is reserved for a general call
    if (u8AddrCpy > 127) // valid addresses are 7-bits only!
        return 0;

    I2C_vidSetValue( (u8AddrCpy << 1) | 0b00000001 ); // load address in data register (read mode)

    BIT_CLEAR(I2C_CTRL, 5); // clear Start flag

    I2C_vidActionInit(); // reset action flag to initiate new one
    I2C_vidWaitWhileBusy(); // wait while busy

    return (I2C_enumMasterGetLastAction() == I2C_master_action_SLA_R_transmitted_ACK);
}

u8 I2C_u8MasterGetDataByTrigger(const u8 u8isAckCpy) // step 3)
{
    // turn on/off ACK bit:
    // turn on to send ACK
    // turn off to send NACK (last byte)
    if (u8isAckCpy)
        I2C_vidEnableAck();
    else
        I2C_vidDisableAck();

    I2C_vidActionInit(); // reset action flag to initiate new one
    I2C_vidWaitWhileBusy(); // wait while busy

    return I2C_DATA;
}
// ***************** //

void I2C_vidMasterStopCond(const u8 u8isAckCpy) // step 4)
{
    // wait for pending actions
    // after (SLA+R then get_data_by_trigger()), master sends ACK/NACK,
    // then slave must "handshake" (release) the bus (tell the master that it received the (N)ACK),
    // and before that, Stop condition MUST NOT be issued.
    I2C_vidWaitWhileBusy();

    BIT_SET(I2C_CTRL, 4); // set Stop flag

    BIT_CLEAR(I2C_CTRL, 5); // clear Start flag

    // turn on/off ACK bit for next round/turn
    // this MUST be after setting stop flag
    if (u8isAckCpy)
        I2C_vidEnableAck();
    else
        I2C_vidDisableAck();

    I2C_vidActionInit(); // reset action flag to initiate new one
    while (BIT_GET(I2C_CTRL, 4)) // wait while Stop bit = 1
    {}
}
// -------------- //


// --- Slave --- //
void I2C_vidSlaveInit(const u8 u8Addr, const u8 u8isRespondGenCall) // step 0)
{
    I2C_vidDisable();

    I2C_vidEnableAck(); // enable ACK bit

    // reset callback functions
    // slave callbacks
    I2C_CB_slave_Stop_or_ReStart_received = 0;
    I2C_CB_slave_SLA_W_received_ACK = 0;
    I2C_CB_slave_SLA_R_received_ACK = 0;
    I2C_CB_slave_General_call_received_ACK = 0;
    I2C_CB_slave_data_received_after_SLA_W_ACK = 0;
    I2C_CB_slave_data_received_after_SLA_W_NACK = 0;
    I2C_CB_slave_data_received_after_General_call_ACK = 0;
    I2C_CB_slave_data_received_after_General_call_NACK = 0;
    I2C_CB_slave_Data_transmitted_ACK = 0;
    I2C_CB_slave_Data_transmitted_NACK = 0;
    I2C_CB_slave_Data_transmitted_NO_TWEA_ACK = 0;
    I2C_CB_slave_Arbitration_lost_own_SLA_W_received_ACK = 0;
    I2C_CB_slave_Arbitration_lost_own_SLA_R_received_ACK = 0;
    I2C_CB_slave_Arbitration_lost_General_call_ACK = 0;

    // error callbacks
    I2C_CB_No_relevant_state_info = 0;

    I2C_vidSetDeviceAddr(u8Addr, u8isRespondGenCall);

    // deactivate internal pull-ups
    DIO_vidSet_pinDirection(port_C, 0, INPUT); // SCL
    DIO_vidDeactivate_pinPullUp(port_C, 0);
    DIO_vidSet_pinDirection(port_C, 1, INPUT); // SDA
    DIO_vidDeactivate_pinPullUp(port_C, 1);

    I2C_vidEnable();

    I2C_isSlave = 1;
}

u8 I2C_u8SlaveSendDataByTrigger(const u8 u8DataCpy)
{// when master requests data (master sends SLA+R)
    I2C_vidSetValue(u8DataCpy); // load data in data register

    I2C_vidActionInit(); // reset action flag to initiate new one
    I2C_vidWaitWhileBusy(); // wait while busy

    register I2C_action_t result = I2C_enumMasterGetLastAction();

    return (   (result == I2C_slave_action_Data_transmitted_ACK)
            || (result == I2C_slave_action_Data_transmitted_NO_TWEA_ACK));
}

void I2C_vidSlaveRecover(void)
{
    // set Stop flag
    BIT_SET(I2C_CTRL, 4);

    // clear Start flag
    BIT_CLEAR(I2C_CTRL, 5);

    // enable ACK bit
    I2C_vidEnableAck();

    I2C_vidActionInit(); // reset action flag to initiate new one
    I2C_vidWaitWhileBusy(); // wait while busy
}
// ------------- //


ISR(TWI_vect)
{
    register I2C_next_action_t next_action = I2C_next_action_NONE;

    switch (I2C_enumMasterGetLastAction())
    {
        // master
        case I2C_master_action_Start:
            if (I2C_CB_master_Start)
                next_action = I2C_CB_master_Start();
        break;

        case I2C_master_action_Repeated_Start:
            if (I2C_CB_master_ReStart)
                next_action = I2C_CB_master_ReStart();
        break;

        case I2C_master_action_SLA_W_transmitted_ACK:
            if (I2C_CB_master_SLAW_transmitted_ACK)
                next_action = I2C_CB_master_SLAW_transmitted_ACK();
        break;

        case I2C_master_action_SLA_W_transmitted_NACK:
            if (I2C_CB_master_SLAW_transmitted_NACK)
                next_action = I2C_CB_master_SLAW_transmitted_NACK();
        break;

        case I2C_master_action_SLA_R_transmitted_ACK:
            if (I2C_CB_master_SLAR_transmitted_ACK)
                next_action = I2C_CB_master_SLAR_transmitted_ACK();
        break;

        case I2C_master_action_SLA_R_transmitted_NACK:
            if (I2C_CB_master_SLAR_transmitted_NACK)
                next_action = I2C_CB_master_SLAR_transmitted_NACK();
        break;

        case I2C_master_action_Data_transmitted_ACK:
            if (I2C_CB_master_Data_transmitted_ACK)
                next_action = I2C_CB_master_Data_transmitted_ACK();
        break;

        case I2C_master_action_Data_transmitted_NACK:
            if (I2C_CB_master_Data_transmitted_NACK)
                next_action = I2C_CB_master_Data_transmitted_NACK();
        break;

        case I2C_master_action_Data_received_ACK:
            if (I2C_CB_master_Data_received_ACK)
                next_action = I2C_CB_master_Data_received_ACK();
        break;

        case I2C_master_action_Data_received_NACK:
            if (I2C_CB_master_Data_received_NACK)
                next_action = I2C_CB_master_Data_received_NACK();
        break;

        case I2C_master_action_Arbitration_lost:
            if (I2C_CB_master_Arbitration_lost)
                next_action = I2C_CB_master_Arbitration_lost();
        break;


        // slave
        case I2C_slave_action_Stop_or_ReStart_received:
            if (I2C_CB_slave_Stop_or_ReStart_received)
                next_action = I2C_CB_slave_Stop_or_ReStart_received();
        break;

        case I2C_slave_action_SLA_W_received_ACK:
            if (I2C_CB_slave_SLA_W_received_ACK)
                next_action = I2C_CB_slave_SLA_W_received_ACK();
        break;

        case I2C_slave_action_SLA_R_received_ACK:
            if (I2C_CB_slave_SLA_R_received_ACK)
                next_action = I2C_CB_slave_SLA_R_received_ACK();
        break;

        case I2C_slave_action_General_call_received_ACK:
            if (I2C_CB_slave_General_call_received_ACK)
                next_action = I2C_CB_slave_General_call_received_ACK();
        break;

        case I2C_slave_action_data_received_after_SLA_W_ACK:
            if (I2C_CB_slave_data_received_after_SLA_W_ACK)
                next_action = I2C_CB_slave_data_received_after_SLA_W_ACK();
        break;

        case I2C_slave_action_data_received_after_SLA_W_NACK:
            if (I2C_CB_slave_data_received_after_SLA_W_NACK)
                next_action = I2C_CB_slave_data_received_after_SLA_W_NACK();
        break;

        case I2C_slave_action_data_received_after_General_call_ACK:
            if (I2C_CB_slave_data_received_after_General_call_ACK)
                next_action = I2C_CB_slave_data_received_after_General_call_ACK();
        break;

        case I2C_slave_action_data_received_after_General_call_NACK:
            if (I2C_CB_slave_data_received_after_General_call_NACK)
                next_action = I2C_CB_slave_data_received_after_General_call_NACK();
        break;

        case I2C_slave_action_Data_transmitted_ACK:
            if (I2C_CB_slave_Data_transmitted_ACK)
                next_action = I2C_CB_slave_Data_transmitted_ACK();
        break;

        case I2C_slave_action_Data_transmitted_NACK:
            if (I2C_CB_slave_Data_transmitted_NACK)
                next_action = I2C_CB_slave_Data_transmitted_NACK();
        break;

        case I2C_slave_action_Data_transmitted_NO_TWEA_ACK:
            if (I2C_CB_slave_Data_transmitted_NO_TWEA_ACK)
                next_action = I2C_CB_slave_Data_transmitted_NO_TWEA_ACK();
        break;

        case I2C_slave_action_Arbitration_lost_own_SLA_W_received_ACK:
            if (I2C_CB_slave_Arbitration_lost_own_SLA_W_received_ACK)
                next_action = I2C_CB_slave_Arbitration_lost_own_SLA_W_received_ACK();
        break;

        case I2C_slave_action_Arbitration_lost_own_SLA_R_received_ACK:
            if (I2C_CB_slave_Arbitration_lost_own_SLA_R_received_ACK)
                next_action = I2C_CB_slave_Arbitration_lost_own_SLA_R_received_ACK();
        break;

        case I2C_slave_action_Arbitration_lost_General_call_ACK:
            if (I2C_CB_slave_Arbitration_lost_General_call_ACK)
                next_action = I2C_CB_slave_Arbitration_lost_General_call_ACK();
        break;


        // error
        case I2C_action_Bus_error_illegal_Start_or_Stop:
            next_action = I2C_next_action_Stop_condition;
        break;

        case I2C_action_No_relevant_state_info:
            if (I2C_CB_No_relevant_state_info)
            	I2C_CB_No_relevant_state_info();
        break;
    }

//    register u8 isAck_ = I2C_u8isAckEnabled();

    switch (next_action)
    {
        case I2C_next_action_ACK:
            // clear Stop flag
            BIT_CLEAR(I2C_CTRL, 4);

            // clear Start flag
            BIT_CLEAR(I2C_CTRL, 5);

            // turn on ACK bit
            I2C_vidEnableAck();

            // reset action flag to initiate new one
            I2C_vidActionInit();
            I2C_vidWaitWhileBusy();
        break;

        case I2C_next_action_NACK:
            // clear Stop flag
            BIT_CLEAR(I2C_CTRL, 4);

            // clear Start flag
            BIT_CLEAR(I2C_CTRL, 5);

            // turn off ACK bit
            I2C_vidDisableAck();

            // reset action flag to initiate new one
            I2C_vidActionInit();
            I2C_vidWaitWhileBusy();
        break;

        case I2C_next_action_Stop_condition:
            // set Stop flag
            BIT_SET(I2C_CTRL, 4);

            // clear Start flag
            BIT_CLEAR(I2C_CTRL, 5);

            // reset action flag to initiate new one
            I2C_vidActionInit();
            I2C_vidWaitWhileBusy();
        break;

        case I2C_next_action_Start_condition:
            // set Start flag
            BIT_SET(I2C_CTRL, 5);

            // clear Stop flag
            BIT_CLEAR(I2C_CTRL, 4);

            // reset action flag to initiate new one
            I2C_vidActionInit();
            I2C_vidWaitWhileBusy();
        break;

        case I2C_next_action_Stop_then_Start_conditions:
            // set Stop flag
            BIT_SET(I2C_CTRL, 4);

            // set Start flag
            BIT_SET(I2C_CTRL, 5);

            // reset action flag to initiate new one
            I2C_vidActionInit();
            I2C_vidWaitWhileBusy();
        break;

        default:
        case I2C_next_action_NONE:

        break;
    }

//    if (isAck_)
//        I2C_vidEnableAck();
//    else
//        I2C_vidDisableAck();

} // ISR()

